# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = Admin.new(
    :email                 => "info@rocopartners.com",
    :password              => "roco12345678",
    :password_confirmation => "roco12345678",
    :company_id => 3
)
admin.save!

kim = Admin.new(
    :email                 => "kim@rocopartners.com",
    :password              => "roco12345678",
    :password_confirmation => "roco12345678",
    :company_id => 1
)
kim.add_role :superadmin
kim.save!

klara = Admin.new(
    :email                 => "klara@rocopartners.com",
    :password              => "roco12345678",
    :password_confirmation => "roco12345678",
    :company_id => 2
)
klara.save!

roco = Company.new(
    :name                 => "rocopartners",
    :address              => "london",
    :website => "www.rocop.com",
    :stripe_publishable_key => ENV['STRIPE_PUBLISHABLE_KEY'],
    :stripe_secret_key => ENV['STRIPE_SECRET_KEY'],
    :stripe_test_publishable_key => ENV['STRIPE_PUBLISHABLE_KEY'],
    :stripe_test_secret_key => ENV['STRIPE_SECRET_KEY'],
)
roco.save!

homeserve = Company.new(
    :name                 => "homeserve",
    :address              => "berlin",
    :website => "www.homeserve.com",
    :stripe_publishable_key => ENV['STRIPE_PUBLISHABLE_KEY'],
    :stripe_secret_key => ENV['STRIPE_SECRET_KEY'],
    :stripe_test_publishable_key => ENV['STRIPE_PUBLISHABLE_KEY'],
    :stripe_test_secret_key => ENV['STRIPE_SECRET_KEY']
)
homeserve.save!

bg = Company.new(
    :name                 => "british gas",
    :address              => "stockholm",
    :website => "www.bg.com",
    :stripe_publishable_key => ENV['STRIPE_PUBLISHABLE_KEY'],
    :stripe_secret_key => ENV['STRIPE_SECRET_KEY'],
    :stripe_test_publishable_key => ENV['STRIPE_PUBLISHABLE_KEY'],
    :stripe_test_secret_key => ENV['STRIPE_SECRET_KEY']
)
bg.save!

user = User.new(
    :uid                   => "test",
    :provider              => "email",
    :email                 => "test@rocopartners.com",
    :password              => "test12345678",
    :password_confirmation => "test12345678",
    :company_id => 1,
    :test_user  => true
)
user.save!

gareth = User.new(
    :uid                   => "gareth",
    :provider              => "email",
    :email                 => "gareth@rocopartners.com",
    :password              => "test12345678",
    :password_confirmation => "test12345678",
    :company_id => 2
)
gareth.save!

eugene = User.new(
    :uid                   => "eugene",
    :provider              => "email",
    :email                 => "eugene@rocopartners.com",
    :password              => "test12345678",
    :password_confirmation => "test12345678",
    :company_id => 1
)
eugene.save!


v = Version.new(
    :major                   => 1,
    :minor              => 0,
    :rev                 => 0
)
v.save!