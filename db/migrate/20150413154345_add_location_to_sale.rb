class AddLocationToSale < ActiveRecord::Migration
  def change
    add_column :sales, :lat, :string
    add_column :sales, :long, :string
  end
end
