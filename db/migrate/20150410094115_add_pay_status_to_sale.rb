class AddPayStatusToSale < ActiveRecord::Migration
  def change
    add_column :sales, :token, :string
    add_column :sales, :paid, :boolean
  end
end
