class ChangeSaleSignatureTypeToText < ActiveRecord::Migration
  def change
    change_column :sales, :signature, :text
  end
end
