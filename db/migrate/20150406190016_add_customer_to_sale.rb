class AddCustomerToSale < ActiveRecord::Migration
  def change
    add_column :sales, :postcode, :string
    add_column :sales, :customer_name, :string
    add_column :sales, :address, :string
    add_column :sales, :city, :string
    add_column :sales, :email, :string
  end
end
