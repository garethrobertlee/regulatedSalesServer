class AddSecretStripeKeyToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :stripe_secret_key, :string
  end
end
