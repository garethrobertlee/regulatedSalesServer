class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.string :name
      t.string :filename
      t.string :signature
      t.string :description
      t.boolean :hasvideo
      t.string :product
      t.string :customer

      t.timestamps
    end
  end
end
