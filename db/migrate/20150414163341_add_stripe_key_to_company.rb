class AddStripeKeyToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :stripe_publishable_key, :string
  end
end
