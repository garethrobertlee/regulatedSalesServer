class AddStripeKeyToSale < ActiveRecord::Migration
  def change
    add_column :sales, :stripe_publishable_key, :string
  end
end
