class AddMoneyToSale < ActiveRecord::Migration
  def change
    add_column :sales, :excess, :numeric
    add_column :sales, :extras, :numeric
    add_column :sales, :plan_id, :integer
    add_column :sales, :product_id, :integer
    add_column :sales, :amount, :numeric
  end
end
