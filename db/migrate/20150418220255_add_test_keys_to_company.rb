class AddTestKeysToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :stripe_test_publishable_key, :string
    add_column :companies, :stripe_test_secret_key, :string
  end
end
