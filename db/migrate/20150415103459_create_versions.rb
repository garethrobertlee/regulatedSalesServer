class CreateVersions < ActiveRecord::Migration
  def change
    create_table :versions do |t|
      t.integer :major
      t.integer :minor
      t.integer :rev
      t.datetime :version_timestamp
      t.text :notes

      t.timestamps
    end
  end
end
