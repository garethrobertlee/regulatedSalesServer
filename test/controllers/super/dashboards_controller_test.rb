require 'test_helper'

class Super::DashboardsControllerTest < ActionController::TestCase
  setup do
    @super_dashboard = super_dashboards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:super_dashboards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create super_dashboard" do
    assert_difference('Super::Dashboard.count') do
      post :create, super_dashboard: {  }
    end

    assert_redirected_to super_dashboard_path(assigns(:super_dashboard))
  end

  test "should show super_dashboard" do
    get :show, id: @super_dashboard
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @super_dashboard
    assert_response :success
  end

  test "should update super_dashboard" do
    patch :update, id: @super_dashboard, super_dashboard: {  }
    assert_redirected_to super_dashboard_path(assigns(:super_dashboard))
  end

  test "should destroy super_dashboard" do
    assert_difference('Super::Dashboard.count', -1) do
      delete :destroy, id: @super_dashboard
    end

    assert_redirected_to super_dashboards_path
  end
end
