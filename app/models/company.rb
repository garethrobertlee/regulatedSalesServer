class Company < ActiveRecord::Base
  resourcify

  has_many :users
  has_many :admins
  has_many :sales, through: :users

  mount_uploader :company_image, CompanyImageUploader
  mount_uploader :logo, LogoUploader
end
