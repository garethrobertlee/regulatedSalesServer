class Ability
  include CanCan::Ability

  def initialize(current_admin)
    # Define abilities for the passed in user here. For example:
    #
    # admin ||= Admin.new # guest user (not logged in)

      if current_admin
        if current_admin.has_role? :superadmin
          can :manage, :all
          can :create, :admin
          # elsif current_admin.has_role? :company_admin
        # elsif current_admin
        #   can :read, :all
        else
          can :read, Company do |company|
            company.id == current_admin.company_id
          end
          # can :read, Sale do | sale |
          #   sale.user.company_id == current_admin.company_id
          # end
          can :create, User
          cannot :index, Company
          can :index, Sale
        end
      end

    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
