class Sale < ActiveRecord::Base

  # has_many :videos

  mount_uploader :clip, VideoUploader

  belongs_to :user
  belongs_to :company
  delegate :company, :to => :user, :allow_nil => true
end
