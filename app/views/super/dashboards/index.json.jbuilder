json.array!(@super_dashboards) do |super_dashboard|
  json.extract! super_dashboard, :id
  json.url super_dashboard_url(super_dashboard, format: :json)
end
