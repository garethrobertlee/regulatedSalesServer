json.array!(@sales) do |sale|
  json.extract! sale, :id, :name, :filename, :signature, :description, :hasvideo, :product, :customer
  json.url sale_url(sale, format: :json)
end
