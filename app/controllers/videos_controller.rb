class VideosController < ApplicationController
  before_action :set_video, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token
  respond_to :html

  def index
    @videos = Video.all
    respond_with(@videos)
  end

  def show
    respond_with(@video)
  end

  def new
    @video = Video.new
    respond_with(@video)
  end

  def edit
  end

  def create
    # @video = Video.new(video_params)
    @video = Video.new(video: params[:file],name: params[:file].original_filename,sale_id:params[:sale_id])

    @video.save
    result = {status: true, message: 'Success', sale: @sale}
    render :json => result
  end

  def update
    @video.update(video_params)
    respond_with(@video)
  end

  def destroy
    @video.destroy
    respond_with(@video)
  end

  private
    def set_video
      @video = Video.find(params[:id])
    end

    def video_params
      params.permit(:name)
    end
end
