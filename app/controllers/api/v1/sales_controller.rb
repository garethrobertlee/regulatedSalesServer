class Api::V1::SalesController < Api::ApiController

  # before_action :authenticate_user!, :except => [:update_with_video,:get_stripe_info,:confirm_and_submit_payment]
  # before_action :authenticate_user!
  # skip_before_filter :verify_authenticity_token
  skip_before_action :authenticate_user!, :only => [:get_latest_version]
  respond_to :json

  def update_with_video
    @sale = Sale.find_by_id(params[:file].original_filename)
    @sale.update(clip:params[:file],hasvideo:true)
    result = {status: true, message: 'Success',sale: @sale}
    render :json => result
  end

  
def get_stripe_info

  result = {status: true,
          message: 'Success',
          stripe_publishable_key: current_user.company.stripe_publishable_key,
          stripe_test_publishable_key: current_user.company.stripe_test_publishable_key
         }

  render :json => result
end



  def confirm_and_submit_payment


    payload = JSON.parse(params['payload'])
    sale  = JSON.parse(params['sale'])


    data = {
        "name" => sale['name'],
        "filename" => sale['name'],
        "signature" => sale['signature'],
        # "description" => sale['description'],
        "description" => JSON.parse(params['sale'])['payload'].to_s,
        "product_id" => sale['product']['id'],
        "plan_id" => sale['payload']['planID'],
        "lat" => sale['lat'],
        "long" => sale['lng'],
        "stripe_publishable_key" => current_user.company.stripe_publishable_key, # maybe we dont need this, its already stored, right?
        "user_id" => current_user.id
    }

    if sale["id"]
      @sale = Sale.find_by(id:sale["id"])
      @sale.update_attributes(data)
      salestatus = 21
    else
      @sale = Sale.new(data)
      if @sale.save
        salestatus = 21
      else
        salestatus = 22
      end
    end

    if payload['token'] != ""

      # Create the charge on Stripe's servers - this will charge the user's card
      begin

        token = payload['token']
        amount = payload['amount'] # in cents

        Stripe.api_key = ENV['STRIPE_SECRET_KEY']

        # Charge the Card
        Stripe::Charge.create(
            :amount => amount, # in cents
            :currency => "gbp",
            :card => token
        )
       paymentstatus = 31

      rescue Stripe::CardError => e
        # The card has been declined

        paymentstatus = 32
        # render :json => result

      rescue Exception => e2
        # The card has been declined
        # The card has been declined or unexpected error

        paymentstatus = 33
          # render :json => result
      end


      if paymentstatus == 31
        if salestatus == 21
          @sale.update_attributes(paid:true,token:payload['token'])
          result = {status: 10, message: 'Success', sale: @sale}
          render :json => result
        else
          result = {status: 11, message: 'payment success, sale not saved'}
          render :json => result
        end
      end
    else
      paymentstatus = 34
    end

    if paymentstatus == 32 || paymentstatus == 33 || paymentstatus == 34
      if salestatus == 21
        result = {status: 12, message: 'payment not taken', sale: @sale}
        render :json => result
      else
        result = {status: 13, message: 'payment not taken, sale not saved'}
        render :json => result
      end
    end
  end

  def get_latest_version

    latest_version = Version.all.order(major: :desc).order(minor: :desc).order(rev: :desc).first

    render :json => latest_version

  end

  def sale_params
    params.require(:sale).permit(:name, :filename, :product_id, :customer, :video, :paid, :user_id, :excess, :extras, :plan_id, :signature, :lat, :long, :description, :hasvideo, :amount, :customer_name, :address, :email, :postcode, :city)
  end
end
