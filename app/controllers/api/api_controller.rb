class Api::ApiController < ApplicationController
  include DeviseTokenAuth::Concerns::SetUserByToken
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #skip_before_filter :verify_authenticity_token
  before_action  :authenticate_user!
  skip_before_action  :authenticate_admin!

end
