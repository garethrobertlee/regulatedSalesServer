class DashboardController < ApplicationController

  def index
    @admins = Admin.all
    @roles = Role.all
    @companies = Company.all
  end

  def dashboard_params
    params.require(:dashboard).permit(:email)
  end
end
