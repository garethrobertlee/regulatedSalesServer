class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  respond_to :html

  def new
    @company = Company.new
    respond_with(@company)

  end

  def index
    @companies = Company.all
    # @company = Company.find(params[:id])
    @admins = Admin.all
    @sales = Sale.all
    @users = User.all

    cities = []
    sales = Sale.all
    sales.each do |sale|
      if sale.description
        city = JSON.parse(sale.description.gsub('=>', ':'))["city"]
        cities << city
      end
    end
    @city_count = Hash.new 0
    cities.each do |city|
      @city_count[city] += 1
    end

    # countries = []
    # sales = Sale.all
    # sales.each do |sale|
    #   if sale.description
    #     country = JSON.parse(sale.description.gsub('=>', ':'))["city"]
    #     countries << country
    #   end
    # end
    # @country_count = Hash.new 0
    # countries.each do |country|
    #   @country_count[country] += 1
    # end

  end

  def create
    @company = Company.new(company_params)
    if @company.save
    @company.admins << Admin.create(email:params[:company][:admins][:email],password:params[:company][:admins][:password],password_confirmation:params[:company][:admins][:password_confirmation],company_id:@company.id)
    end
    respond_with(@company)
  end

  def show
      @company = Company.find(params[:id])
      if @user
        @user = User.find(params[:id])
      end
      @all_sales = @company.sales.all
      @q = @all_sales.search(params[:q])
      @sales = @q.result(distinct: true).order("created_at DESC").page(params[:page])
  end

  def edit
    @company = Company.find(params[:id])
  end

  def update
    @company.update(company_params)
    if @company.save
      @company.admins << Admin.create(email:params[:company][:admins][:email],password:params[:company][:admins][:password],password_confirmation:params[:company][:admins][:password_confirmation],company_id:@company.id)
    end
    respond_with(@company)

  end

  private
  def set_company
    @company = Company.find(params[:id])
  end

  def company_params
    params.require(:company).permit(:name, :id, :email, :website,:stripe_publishable_key, :stripe_secret_key, :password, :password_confirmation, :company_image, :logo)
  end
end
