class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :null_session

  #before_action  :authenticate_admin!

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to new_admin_session_path
  end

  def after_sign_in_path_for(resource)
    if resource[:id] == 2
    companies_path
    else
      company_path(resource.company_id)
      end
  end

  def current_ability
    @current_ability ||= Ability.new(current_admin)
  end

end
