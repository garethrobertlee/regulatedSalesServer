class UsersController < ApplicationController
  before_filter :authenticate_admin!
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  respond_to :html
  load_and_authorize_resource

  def index
    @users = User.all
  end

  def show
    respond_with(@user)
  end

  def new
    @user = User.new
    respond_with(@user)
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    @user.provider = 'email'
    @user.uid = @user.email
    @user.save
    respond_with(@user)
    # redirect_to companies_path
  end

  def update
    @user.update(user_params)
    respond_with(@user)
    #redirect_to users_path
  end

  def destroy
    @user.destroy
    respond_with(@user)
  end

  private
  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params[:user].permit(:email, :password, :password_confirmation, :company_id)
  end
end
