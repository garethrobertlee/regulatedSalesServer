class Super::DashboardController < ApplicationController
  before_action :set_super_dashboard, only: [:show, :edit, :update, :destroy]
  # load_and_authorize_resource
  respond_to :html

  def index
    @companies = Company.all
    # respond_with(@companies)
    @admins = Admin.all

    @sales = Sale.all

    @users = User.all

  end

  def show

    @company = Company.find(params[:id])
    respond_with(@company)
  end

  def new
    @company = Company.new
    respond_with(@company)
  end

  def edit
  end

  def create
    @company = Company.new(company_params)
    @company.save
    respond_with(@company)
  end

  def update
    @company.update(company_params)
    respond_with(@company)
  end

  def destroy
    @company.destroy
    respond_with(@company)
  end

  private
    def set_company
      @company = Company.find(params[:id])
    end

  def company_params
    params.require(:companies).permit(:name, :id)
  end
end
