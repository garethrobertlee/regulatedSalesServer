class SalesController < ApplicationController
  before_action :set_sale, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  respond_to :html

  def index
    if current_admin.has_role? :superadmin
      @q = Sale.ransack(params[:q])
      @sales = @q.result(distinct: true).order("created_at DESC").page params[:page]
    else
      @sales_company = current_admin.company.sales
      @q = @sales_company.ransack(params[:q])
      @sales = @q.result(distinct: true).order("created_at DESC").page params[:page]
    end

  end

  def show
    respond_with(@sale)
  end

  def new
    @sale = Sale.new
    respond_with(@sale)
  end

  def edit
  end

  def create
    # @sale = Sale.new(sale_params)
    @sale = Sale.new(params)
    @sale.save
    respond_with(@sale)
  end

  def update
    @sale.update(sale_params)
    respond_with(@sale)
  end

  def destroy
    @sale.destroy
    respond_with(@sale)
  end

  def make_payment
    token = params[:stripeToken]
    amount = params[:amount]
  end

  private
    def set_sale
      @sale = Sale.find(params[:id])
    end

    def sale_params
      params.permit(:name, :filename, :signature, :description, :hasvideo, :product, :customer)
    end
end
