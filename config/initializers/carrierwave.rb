CarrierWave.configure do |config|
  config.fog_credentials = {
      provider:           'Rackspace',
      rackspace_username: 'rocodevelopers',
      rackspace_api_key:  ENV['rackspace_api_key'],
      rackspace_auth_url: Fog::Rackspace::UK_AUTH_ENDPOINT,
      rackspace_region:   :lon
  }
  config.fog_directory = 'regulated_sales'
end
